package io.github.juanhdev.blog_pessoal.model;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UsuarioTest {

  public Usuario usuario;
  public Usuario nullUser = new Usuario();

  @Autowired
  private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();

  Validator validator = factory.getValidator();

  @BeforeEach
  public void start() {
    LocalDate date = LocalDate.parse(
      "2000-07-22",
      DateTimeFormatter.ofPattern("yyyy-MM-dd")
    );
    usuario =
      new Usuario(
        0L,
        "João da Silva",
        "jaozin",
        "joao@email.com.br",
        "134652789012",
        date
      );
  }

  @Test
  @DisplayName("✔ Valida Atributos Não Nulos")
  void testValidaAtributos() {
    Set<ConstraintViolation<Usuario>> violation = validator.validate(usuario);
    System.out.println(violation.toString());
    assertTrue(violation.isEmpty());
  }

  @Test
  @DisplayName("✖ Não Valida Atributos Nulos")
  void testNaoValidaAtributos() {
    Set<ConstraintViolation<Usuario>> violation = validator.validate(usuario);
    System.out.println(violation.toString());
    assertTrue(violation.isEmpty());
  }
}
