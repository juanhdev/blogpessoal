package io.github.juanhdev.blog_pessoal.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import io.github.juanhdev.blog_pessoal.model.Usuario;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UsuarioRepositoryTest {

  @Autowired
  private UsuarioRepository repo;

  @BeforeAll
  void start() {
    LocalDate data = LocalDate.parse("2000-07-22", DateTimeFormatter.ofPattern("yyyy-MM-dd"));

    Usuario usuario = new Usuario(0, "João da Silva", "jaozin", "joao@email.com.br", "134652789012", data);

    if (!repo.findByUsername(usuario.getUsername()).isPresent())
      repo.save(usuario);

    usuario = new Usuario(0, "Manuel da Silva", "manual", "manuel@email.com.br", "134652780912", data);
    if (!repo.findByUsername(usuario.getUsername()).isPresent())
      repo.save(usuario);

      usuario = new Usuario(0, "Frederico da Silva", "fred", "frederico@email.com.br", "134652780912", data);
    if (!repo.findByUsername(usuario.getUsername()).isPresent())
      repo.save(usuario);

      usuario = new Usuario(0, "Paulo Antunes", "pau", "paulo@email.com.br", "134652780912", data);
    if (!repo.findByUsername(usuario.getUsername()).isPresent())
      repo.save(usuario);
  }

  @Test
  @DisplayName("💾 Retorna o nome")
  public void findByNomeRetornaNome() {
    Usuario usuario = repo.findByName("João da Silva");
    assertEquals("João da Silva", usuario.getName());
  }

  @Test
  @DisplayName("💾 Retorna 3 usuarios")
  public void findAllByNomeContainingIgnoreCaseRetornaTresUsuarios() {
    List<Usuario> listaDeUsuarios = repo.findAllByNameContainingIgnoreCase("Silva");
    assertEquals(3, listaDeUsuarios.size());
  }

  @AfterAll
  public void end() {
    System.out.println("Teste Finalizado!");
  }
}
