package io.github.juanhdev.blog_pessoal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "post")
public class BlogPost {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @NotNull(message = "Title required")
  @Size(min = 10, max = 100, message = "Min: 5, Max: 100")
  private String title;

  @NotNull(message = "Body required")
  @Size(min = 25, max = 510, message = "Min: 25, Max: 510")
  private String body;

  @Temporal(TemporalType.TIMESTAMP)
  private Date date = new java.sql.Date(System.currentTimeMillis());

  @ManyToOne
  @JsonIgnoreProperties("blogPost")
  private Topic topic;

  @ManyToOne
  @JsonIgnoreProperties("blogPost")
  private Usuario usuario;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public Topic getTheme() {
    return topic;
  }

  public void setTheme(Topic topic) {
    this.topic = topic;
  }

  public Usuario getUser() {
    return usuario;
  }

  public void setUser(Usuario user) {
    this.usuario = user;
  }
}
