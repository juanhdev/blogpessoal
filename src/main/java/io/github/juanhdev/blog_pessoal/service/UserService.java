package io.github.juanhdev.blog_pessoal.service;

import io.github.juanhdev.blog_pessoal.model.Usuario;
import io.github.juanhdev.blog_pessoal.model.UsuarioLogin;
import io.github.juanhdev.blog_pessoal.repository.UsuarioRepository;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.Period;
import java.util.Optional;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class UserService {

  @Autowired
  private UsuarioRepository repo;

  public Usuario registerUser(Usuario user) {
    if (repo.findByUsername(user.getUsername()).isPresent())
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Usuário já existe", null);

    int age = Period.between(user.getBirthdate(), LocalDate.now()).getYears();

    if (age < 18)
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Deve ser maior de 18", null);

    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    String passwordEncoder = encoder.encode(user.getPassword());
    user.setPassword(passwordEncoder);

    return repo.save(user);
  }

  public Optional<Usuario> updateUser(Usuario user) {
    if (repo.findById(user.getId()).isPresent()) {
      Optional<Usuario> findUser = repo.findByUsername(user.getUsername());
      Optional<Usuario> findUserEmail = repo.findByEmailIgnoreCase(user.getEmail());

      if (findUserEmail.isPresent()) {
        if (findUser.get().getId() != user.getId()) {
          throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Email já existe", null);
        }
      }

      if (findUser.isPresent()) {
        if (findUser.get().getId() != user.getId()) {
          throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Nome de usuário já existe", null);
        }
      }
      int age = Period.between(user.getBirthdate(), LocalDate.now()).getYears();

      if (age < 18)
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Deve ser maior de 18", null);

      BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

      String passwordEncoder = encoder.encode(user.getPassword());
      user.setPassword(passwordEncoder);

      return Optional.of(repo.save(user));
    } else {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuário não encontrado", null);
    }
  }

  public Optional<UsuarioLogin> loginUser(Optional<UsuarioLogin> userLogin) {
    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    Optional<Usuario> user = repo.findByUsername(userLogin.get().getUsername());

    if (user.isPresent()) {
      if (encoder.matches(userLogin.get().getPassword(), user.get().getPassword())) {
        String auth = userLogin.get().getUsername() + ":" + userLogin.get().getPassword();
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(StandardCharsets.US_ASCII));
        String authHeader = "Basic " + new String(encodedAuth);

        userLogin.get().setToken(authHeader);
        userLogin.get().setUsername(user.get().getUsername());
        userLogin.get().setPassword(user.get().getPassword());

        return userLogin;
      }
    }
    throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Usuário ou senha inválidos!", null);
  }
}
