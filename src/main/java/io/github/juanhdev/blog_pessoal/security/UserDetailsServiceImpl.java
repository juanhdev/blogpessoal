package io.github.juanhdev.blog_pessoal.security;

import io.github.juanhdev.blog_pessoal.model.Usuario;
import io.github.juanhdev.blog_pessoal.repository.UsuarioRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

  @Autowired
  private UsuarioRepository repo;

  @Override
  public UserDetails loadUserByUsername(String username)
    throws UsernameNotFoundException {
    Optional<Usuario> usuario = repo.findByUsername(username);
    usuario.orElseThrow(
      () -> new UsernameNotFoundException(username + " not found.")
    );

    return usuario.map(UserDetailsImpl::new).get();
  }
}
