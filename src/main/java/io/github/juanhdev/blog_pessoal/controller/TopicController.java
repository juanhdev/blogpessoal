package io.github.juanhdev.blog_pessoal.controller;

import io.github.juanhdev.blog_pessoal.model.Topic;
import io.github.juanhdev.blog_pessoal.repository.TopicRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/topics")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TopicController {

  @Autowired
  private TopicRepository repo;

  @GetMapping
  public ResponseEntity<List<Topic>> getAll() {
    return ResponseEntity.ok(repo.findAll());
  }

  @GetMapping("/{id}")
  public ResponseEntity<Topic> getById(@PathVariable long id) {
    return repo
      .findById(id)
      .map(ResponseEntity::ok)
      .orElse(ResponseEntity.notFound().build());
  }

  @GetMapping("/title/{title}")
  public ResponseEntity<Topic> getByTitle(@PathVariable String title) {
    return ResponseEntity.ok(repo.findAllByNameContainingIgnoreCase(title));
  }

  @GetMapping("/description/{description}")
  public ResponseEntity<Topic> getByDescription(
    @PathVariable String description
  ) {
    return ResponseEntity.ok(
      repo.findAllByDescriptionContainingIgnoreCase(description)
    );
  }

  @PostMapping
  public ResponseEntity<Topic> createTheme(@RequestBody Topic topic) {
    return ResponseEntity.status(HttpStatus.CREATED).body(repo.save(topic));
  }

  @PutMapping
  public ResponseEntity<Topic> editTheme(@RequestBody Topic topic) {
    Optional<Topic> themeUpdate = repo.findById(topic.getId());

    if (themeUpdate.isPresent()) {
      return ResponseEntity.status(HttpStatus.OK).body(repo.save(topic));
    } else {
      throw new ResponseStatusException(
        HttpStatus.NOT_FOUND,
        "Tema não encontrado",
        null
      );
    }
  }

  @DeleteMapping("/{id}")
  public void deleteTheme(@PathVariable long id) {
    Optional<Topic> theme = repo.findById(id);
    if (theme.isPresent()) {
      repo.deleteById(id);
    } else {
      throw new ResponseStatusException(
        HttpStatus.NOT_FOUND,
        "Tema não encontrado",
        null
      );
    }
  }
}
