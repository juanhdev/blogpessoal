package io.github.juanhdev.blog_pessoal.repository;

import io.github.juanhdev.blog_pessoal.model.Usuario;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
  Optional<Usuario> findByUsername(String username);
  Optional<Usuario> findByEmailIgnoreCase(String email);

  List<Usuario> findAllByNameContainingIgnoreCase(String name);

  Usuario findByName(String name);
}
