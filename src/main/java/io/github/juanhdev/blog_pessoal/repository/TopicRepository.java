package io.github.juanhdev.blog_pessoal.repository;

import io.github.juanhdev.blog_pessoal.model.Topic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicRepository extends JpaRepository<Topic, Long> {
  Topic findAllByNameContainingIgnoreCase(String title);

  Topic findAllByDescriptionContainingIgnoreCase(String description);
}
